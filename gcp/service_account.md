## Service accounts

The 

To create a new service account:

```
gcloud iam service-accounts create SERVICE_ACCOUNT_NAME

```

To grant a given service account a policy:

```
gcloud projects add-iam-policy-binding PROJECT_ID --member="serviceAccount:SERVICE_ACCOUNT_NAME@PROJECT_ID.iam.gserviceaccount.com" --role=ROLE
```


To activate a given service account

```
gcloud auth activate-service-account --project=PROJECT_ID --key-file=KEYFILE_PATH
```

To allow a member to use service account roles:

```
gcloud iam service-accounts add-iam-policy-binding SERVICE_ACCOUNT_NAME@PROJECT_ID.iam.gserviceaccount.com --member="user:USER_EMAIL" --role=roles/iam.serviceAccountUser
```


Source: [VertexAI Authentication Documentation](https://cloud.google.com/vertex-ai/docs/workbench/reference/authentication)
